from . import views
from django.urls import path

app_name = 'blog_app'
urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('<int:post_id>', views.post_detail, name="post_detail"),
    path('<slug:category_slug>', views.post_list, name='post_list_by_category'),
    path('share/<int:post_id>', views.post_share, name='post_share'),
    # path('search/',views.search_post,name='search_post')
]
