from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=80)
    slug = models.SlugField(max_length=80)

    class Meta:
        ordering = ('name',)
        db_table = 'category'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('blog_app:post_list_by_category', args=[self.slug])


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='published')


class Post(models.Model):
    published = PublishedManager()
    CHOICES = (('draft', 'Draft'), ('published', 'Published'))
    title = models.CharField(max_length=250)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique_for_date='publish')
    author = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    publish = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=50, choices=CHOICES, default='draft')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='posts', blank=True, null=True)
    image = models.ImageField(upload_to='images/%y/%m/%d/', blank=True, null=True,
                              default='images/blank-img.jpg')

    class Meta:
        ordering = ('-created',)
        db_table = 'post'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog_app:post_detail', args=[self.id])


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    active = models.BooleanField(default=False)
    name = models.CharField(max_length=80)
    message = models.TextField()
    email = models.EmailField()
    time = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('time',)
        db_table = 'comment'
