from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('name', 'email', 'message')


class SendEmailForm(forms.Form):
    name = forms.CharField(max_length=50)
    email = forms.EmailField()
    contact_email = forms.EmailField()
    message = forms.CharField(required=False,
                              widget=forms.Textarea(attrs={'class': 'md-textarea form-control', 'rows': '3'}))


class SearchForm(forms.Form):
    query = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': "form-control mr-sm-2", 'type': "text", 'placeholder': "Search", 'aria-label': "Search"}))
