from django.contrib import admin

from blog.models import Post, Comment, Category


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'body', 'author', 'slug', 'publish', 'status')
    list_filter = ('status', 'title', 'author', 'created', 'updated')
    search_fields = ('title', 'body',)
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ('-publish',)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('post', 'name', 'email', 'message', 'time', 'active')
    list_filter = ('post', 'name', 'email', 'time', 'active')
    search_fields = ('name', 'email', 'message')
    ordering = ('time',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    prepopulated_fields = {'slug': ('name',)}
