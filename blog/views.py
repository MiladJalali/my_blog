from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count
from django.shortcuts import render, get_object_or_404

from blog.forms import CommentForm, SendEmailForm, SearchForm
from blog.models import Post, Comment, Category


def post_list(request, category_slug=None):
    query = None
    results = []
    form = SearchForm()
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            search_query = SearchQuery(query)
            search_vector = SearchVector('title', weight='A') + SearchVector('body', weight='B')
            search_rank = SearchRank(search_vector, search_query)
            results = Post.published.annotate(rank=search_rank).filter(rank__gte=0.1).order_by('-rank')
    if category_slug:
        posts = Post.published.filter(category__slug=category_slug)
    else:
        posts = Post.published.all()
    recent_posts = Post.published.all()[:3]
    popular_post = Post.published.annotate(num_comments=Count('comments')).order_by('-num_comments')[:3]
    categories = Category.objects.all()
    paginator = Paginator(posts, 5)
    current_page_number = request.GET.get('page')
    try:
        posts = paginator.page(current_page_number)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'blog/home.html',
                  {'posts': posts, 'recent_posts': recent_posts, 'popular_posts': popular_post,
                   'categories': categories, 'results': results, 'form': form, 'query': query})


def post_detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    recommended_posts = Post.published.filter(category=post.category).exclude(pk=post_id)
    comments = post.comments.filter(active=True)
    new_comment = None
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.active = False
            new_comment.post = post
            new_comment.save()
    else:
        form = CommentForm()
    return render(request, 'blog/post.html',
                  {'post': post, 'form': form, 'new_comment': new_comment, 'comments': comments,
                   'recommended_posts': recommended_posts})


def post_share(request, post_id):
    send = False
    post = get_object_or_404(Post, id=post_id)
    if request.method == 'GET':
        form = SendEmailForm()
    else:
        form = SendEmailForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = "post_share"
            message = "{} with this email: {} send this post ".format(cd['name'], cd[
                'email']) + '\n' + request.build_absolute_uri(post.get_absolute_url())
            send_mail(subject, message, cd['email'], [cd['contact_email'], ])
            send = True

    return render(request, 'blog/share.html', {'form': form, 'send': send})
